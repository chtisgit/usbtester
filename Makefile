CXX = g++
CFLAGS = -std=c++11 -Ilibsodium/src/libsodium/include -O3 -g -Wall
LFLAGS = -Llibsodium/src/libsodium/.libs
LIBS = -lsodium

usbtest: usbtest.o File.o progress.o
	$(CXX) $(LFLAGS) -o $@ $^ -lsodium $(LIBS)

%.o: %.cpp
	$(CXX) $(CFLAGS) -O2 -g -Wall -c -o $@ $<
