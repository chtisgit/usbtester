#include <vector>
#include <utility>
#include <algorithm>

#include <cstdio>
#include <cerrno>
#include <cstring>
#include <cassert>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include <sodium.h>

#include "File.h"
#include "progress.h"

#define Min(A,B) ((A)>(B) ? (B) : (A))

struct Options{
	// block size
	int bs = 512;
	int bufsz = 256*1024;
	int blocks_in_buf;
	bool romode;
} options;

void usage(const char *progname)
{
	static const char *USAGE = 
		"Usage: %s <block device> [-r|-w] [-t test]\n\n"
		"-r \tread-only mode (run all read-only tests) [default]\n"
		"-w \twrite mode (runs all write tests)\n"
		"-t \tspecify a test to be run (see list below)\n"
		"\n"
		"available read-only tests: readonly\n"
		"available writing tests: loop, readwrite\n"
		"\n";
	printf(USAGE,progname);
}

void parseargs(int argc, char **argv)
{
	static const char *OPTSTR = "rwt:";
	int o;
	options.romode = true;
	while((o = getopt(argc,argv,OPTSTR)) != -1){
		switch(o){
		case 'r':
			break;
		case 'w':
			options.romode = false;
			break;
		case 't':
			break;
		default:
			printf("unknown option %c\n",o);
			exit(1);
		}
	}
	options.blocks_in_buf = options.bufsz / options.bs;
	assert(options.bufsz % options.bs == 0);
}

template<typename T, typename U>
std::pair<T&,U&> P(T& a, U& b)
{
	return std::pair<T&,U&>{a,b};
}

void xxd(const char *s, int len)
{
	int i;
	for(i = 0; i < len; i++){
		if(i % 16 == 0){
			printf("%08x: ",i);
		}
		printf("%02x",unsigned(s[i]) & 0xFF);
		if(i % 2 == 1)
			printf(" ");
		if(i % 16 == 15)
			printf("\n");
	}
}

char* align(char *x, size_t a)
{
	size_t xi = size_t(x), m = xi % a;
	char *addr;
	if(m == 0)
		addr = x;
	else
		addr = x - m + a;
	
	assert(addr >= x && addr < x+a);
	return addr;
}

void printhash(const char *fmt, const char *hash, int len)
{
	std::vector<char> b(size_t(len*4/3.0 + 6));
	char *s = sodium_bin2base64(b.data(), b.size(), (unsigned char*)hash, len, sodium_base64_VARIANT_ORIGINAL);
	printf(fmt,s);
}

/*
 * looptest(File&)
 *
 * this function computes a hash of the complete block device and another
 * hash with one sector of size options.bs changed. Then it writes this changes
 * to the block device and hashes it again. This function should be able to detect
 * usb drives whose memory "wraps around"
 *
 */
void looptest(File& dev)
{
	struct{
		char original[crypto_generichash_BYTES];
		crypto_generichash_state ostate;
		char shouldbe[crypto_generichash_BYTES];
		crypto_generichash_state sstate;
		char after[crypto_generichash_BYTES];
		crypto_generichash_state astate;
	} hash;

	enum { ERROR, OK, FAIL_UNCHANGED, FAIL } status = ERROR;

	std::vector<char> unaligned_buffer(options.bufsz + options.bs);
	std::vector<char> unaligned2(options.bs*3);
	char *buf = align(unaligned_buffer.data(),options.bs);
	char *saved = align(unaligned2.data(), options.bs);
	char *inv = saved + options.bs;

	const auto sector = size_t(randombytes_uniform(65536));
	dev.read_sector(sector, saved, options.bs);
	for(int i = 0; i < options.bs; i++){
		inv[i] = ~inv[i];
	}

	crypto_generichash_init(&hash.ostate, nullptr, 0, sizeof(hash.original));
	crypto_generichash_init(&hash.sstate, nullptr, 0, sizeof(hash.shouldbe));

	ProgressBar pbar(60);
	size_t rdbytes = 0;
	size_t totalbytes = dev.size();
	pbar.show(0);
	while(!dev.eof()){
		size_t rd;
		int err;
		P(rd,err) = dev.read(buf, options.bufsz);
		rdbytes += rd;
		if(err != 0){
			printf("error while reading...\n\n");
			return;
		}
		crypto_generichash_update(&hash.ostate, (unsigned char*)buf, rd);
		auto x = sector*options.bs;
		if(x < rdbytes && (x < options.bufsz || x >= rdbytes - options.bufsz)){
			memcpy(buf+(x - (rdbytes-options.bufsz)), inv, options.bs);
		}
		crypto_generichash_update(&hash.sstate, (unsigned char*)buf, rd);
		pbar.show(double(rdbytes)/totalbytes/2);
	}

	crypto_generichash_final(&hash.ostate, (unsigned char*)hash.original, sizeof(hash.original));
	crypto_generichash_final(&hash.sstate, (unsigned char*)hash.shouldbe, sizeof(hash.shouldbe));

	int e = dev.write_sector(sector, inv, options.bs);
	// after this write we need to make sure that the sector is
	// restored before the function exits

	dev.seek(0);
	rdbytes = 0;
	crypto_generichash_init(&hash.astate, nullptr, 0, sizeof(hash.after));
	while(!dev.eof()){
		size_t rd;
		int err;
		P(rd,err) = dev.read(buf, options.bufsz);
		rdbytes += rd;
		if(err != 0){
			printf("error while reading...\n\n");
			goto restore;
		}
		crypto_generichash_update(&hash.astate, (unsigned char*)buf, rd);
		pbar.show(0.5+double(rdbytes)/totalbytes/2);
	}
	crypto_generichash_final(&hash.astate, (unsigned char*)hash.after, sizeof(hash.after));

	if(memcmp(hash.after, hash.shouldbe, sizeof(hash.after)) == 0){
		status = OK;
	}else if(memcmp(hash.after, hash.original, sizeof(hash.after)) == 0){
		status = FAIL_UNCHANGED;
	}else{
		status = FAIL;
	}

restore:
	dev.write_sector(sector, saved, options.bs);

	// print test result after restoring sector
	pbar.show(1);
	if(status == OK){
		printf("LOOPTEST OK\n");
	}else{
		printf("LOOPTEST FAILED\n");
		if(status == FAIL_UNCHANGED){
			printf("the written sector didn't change. Is your drive read-only?\n");
		}
	}
}

void rwtest(File& dev)
{
	std::vector<size_t> sectors(4096);
	std::vector<char> unaligned(sectors.size() * options.bs + options.bs);
	std::vector<char> unaligned2(sectors.size() * options.bs + options.bs);
	std::vector<size_t> errors;
	char *testdata = align(unaligned.data(), options.bs);
	char *saveddata = align(unaligned2.data(), options.bs);
	ProgressBar pbar(60);

	pbar.show(0);
	randombytes_buf(unaligned.data(), unaligned.size());

	// determine the sectors that shall be written
	const auto devsectors = size_t(dev.size() / options.bs);
	const auto inc = size_t(devsectors / sectors.size());
	for(size_t i = 0; i < sectors.size(); i++){
		sectors[i] = i*inc;
		assert(sectors[i] < devsectors);
	}

	// read the current data into saveddata
	for(size_t i = 0; i < sectors.size(); i++){
		dev.read_sector(sectors[i], saveddata+i*options.bs, options.bs);
		pbar.show(double(i)/sectors.size()/5);
	}

	// write sectors with random data
	for(size_t i = 0; i < sectors.size(); i++){
		dev.write_sector(sectors[i], testdata+i*options.bs, options.bs);
		pbar.show((double(i)/sectors.size()+1)/5);
	}

	// check if they changed
	for(size_t i = 0; i < sectors.size(); i++){
		char un[options.bs*2];
		char *buf = align(un, options.bs);

		dev.read_sector(sectors[i], buf, options.bs);
		if(memcmp(buf, testdata+i*options.bs, options.bs) != 0){
			errors.push_back(sectors[i]);
		}
		pbar.show((double(i)/sectors.size()+2)/5);
	}

	// change them back
	for(size_t i = 0; i < sectors.size(); i++){
		dev.write_sector(sectors[i], saveddata+i*options.bs, options.bs);
		pbar.show((double(i)/sectors.size()+3)/5);
	}

	// check if they changed back
	for(size_t i = 0; i < sectors.size(); i++){
		char un[options.bs*2];
		char *buf = align(un, options.bs);

		dev.read_sector(sectors[i], buf, options.bs);
		if(memcmp(buf, saveddata+i*options.bs, options.bs) != 0){
			errors.push_back(sectors[i]);
		}
		pbar.show((double(i)/sectors.size()+4)/5);
	}

	std::sort(errors.begin(), errors.end());
	errors.erase(std::unique(errors.begin(), errors.end()), errors.end());
	pbar.show(1);

	if(errors.empty()){
		printf("RWTEST OK\n");
	}else{
		printf("RWTEST FAILED\n");
		printf("errors in %zu sectors. First sector with error: %zu.\n", errors.size(), errors[0]);
	}
}

bool iszero(char *from, char *to)
{
	while(from != to){
		if(*from != 0)
			return false;
		from++;
	}
	return true;
}

void rotest(File& dev)
{
	char un[options.bufsz + options.bs];
	char *buf = align(un, options.bs);

	constexpr int FINDMIN = 16;
	int found = 0;

	std::vector<char> unaligned_buffer(options.bs*(FINDMIN+1));
	char *pattern = align(unaligned_buffer.data(),options.bs);

	auto devsectors = size_t(dev.size() / options.bs);
	size_t patternstart;
	ProgressBar pbar(60);
	size_t onlyempty = 0, empty = 0;
	auto lastempty = false;

	pbar.show(0);
	for(size_t i = 0; i < devsectors - FINDMIN*2; i++){
		dev.read_sector(i, buf, options.bs);
		if(iszero(buf, buf+options.bs)){
			found = 0;
			empty++;
			if(!lastempty){
				onlyempty = i;
				lastempty = true;
			}
		}else{
			found++;
			lastempty = false;
		}
		if(found == FINDMIN){
			patternstart = i - FINDMIN + 1;
			break;
		}
		pbar.show(double(i)/devsectors/10);
	}
	if(found != FINDMIN){
		printf("error: too little entropy on device for read-only test!\n");
		return;
	}

	// read pattern
	for(size_t i = patternstart; i < patternstart + FINDMIN; i++){
		dev.read_sector(i, pattern+(i-patternstart)*options.bs, options.bs);
	}

	std::vector<size_t> repetition;
	found = 0;
	dev.seek((patternstart+FINDMIN)*options.bs);
	for(size_t i = patternstart+FINDMIN; i < devsectors; i+=options.blocks_in_buf){
		size_t blocks;
		int err;
		P(blocks,err) = dev.read(buf, options.bufsz);
		blocks /= options.bs;

		for(size_t j = 0; j < blocks; j++){
			if(memcmp(buf+j*options.bs, pattern+found*options.bs, options.bs) == 0){
				found++;
			}else{
				found = 0;
			}
			if(iszero(buf+j*options.bs, buf+j*options.bs+options.bs)){
				empty++;
				if(!lastempty){
					onlyempty = i+j;
					lastempty = true;
				}
			}else{
				lastempty = false;
			}
			if(found == FINDMIN){
				found = 0;
				repetition.push_back(i+j-FINDMIN+1);
			}
		}
		pbar.show((double(i)/(devsectors-patternstart-FINDMIN)*9+1)/10);
	}
	pbar.show(1);

	if(repetition.empty()){
		printf("ROTEST OK\n");
	}else{
		printf("ROTEST FAILED\n");
		printf("found %zu repetitions (of sectors %zu - %zu).\n", repetition.size(), patternstart, patternstart+FINDMIN);
		for(size_t i = 0; i < Min(repetition.size(),size_t(5)); i++){
			const auto pos = repetition[i];
			printf(" - sector %zu - %zu\n", pos, pos+FINDMIN);
		}
		if(repetition.size() > 5){
			printf(" ...\n");
			if(repetition.size() > 8){
				for(size_t i = repetition.size()-3; i < repetition.size(); i++){
					const auto pos = repetition[i];
					printf(" - sector %zu - %zu\n", pos, pos+FINDMIN);
				}
			}
		}
		printf("The device is approx. %.2f%% empty (%zu sectors)\n", 100.0*empty/devsectors, empty);
		if(lastempty)
			printf("All blocks after sector %zu are empty\n", onlyempty);
	}
}

void deviceinfo(const char* name, File& dev)
{
	printf("Device: %s\n", name);
	printf("Total sectors: %zu\n",dev.size()/options.bs);
	printf("\n");
}

int main(int argc, char **argv)
{
	if(sodium_init() == -1){
		printf("error: could not initialize libsodium!\n\n");
		return 1;
	}

	if(argc < 2){
		usage(argv[0]);
		return 1;
	}
	parseargs(argc, argv);
	
	if(options.romode){
		File rodev(argv[1], O_DIRECT|O_CLOEXEC|O_SYNC|O_RDONLY);
		deviceinfo(argv[1], rodev);
		printf("R/O TEST:\n");
		rotest(rodev);
	}else{
		File dev(argv[1], O_DIRECT|O_CLOEXEC|O_SYNC|O_RDWR);

		deviceinfo(argv[1], dev);
		printf("LOOP TEST:\n");
		looptest(dev);

		printf("R/W TEST:\n");
		rwtest(dev);
	}

	return 0;
}