#include "progress.h"

#include <cassert>
#include <cstdio>
#include <cmath>

constexpr int ETALEN = 13;

ProgressBar::ProgressBar(size_t lw)
	: perc(0), dperc(0), linewidth(lw), started(false), redraw(true)
{
}

void ProgressBar::update(double progress)
{
	const int pi = progress * 100;

	dperc = progress;

	if(!started){
		started = true;
		starttime = time(nullptr);
	}

	if(pi != perc)
		redraw = true;
	perc = pi;
}

time_t ProgressBar::eta()
{
	time_t diff = time(nullptr) - starttime;
	if(dperc == 0)
		return -1;
	return (1.0/dperc * diff) - diff;
}
size_t ProgressBar::barwidth()
{
	return linewidth - (18+ETALEN);
}
size_t ProgressBar::width()
{
	return linewidth+1;
}
void ProgressBar::sprinteta(char *buf, size_t n)
{
	assert(n >= 11);
	const int digits = n-10;

	const auto e = eta();
	const auto max = time_t(pow(10, digits));
	if(e < 0){
		buf[0] = '\0';
		return;
	}
	if(e >= max){
		snprintf(buf, n, "ETA >%ld sec", max-1);
	}else{
		snprintf(buf, n, "ETA ~%ld sec", e);
	}
}
void ProgressBar::sprint(char *buf)
{
	const int pwi = barwidth();
	char bar[pwi+1], etabuf[ETALEN+1];
	int to = pwi * dperc;
	char last = '\r';

	assert(to >= 0 && to <= pwi);
	for(int i = 0; i < pwi; i++){
		if(i < to)
			bar[i] = '=';
		else
			bar[i] = '.';
	}
	bar[pwi] = '\0';

	if(dperc == 1)
		last = '\n';

	sprinteta(etabuf, ETALEN+1);
	snprintf(buf, width(), "progress %d%% [%s] %s%c", perc, bar, etabuf, last);
}
void ProgressBar::print(bool force)
{
	if(!force && !redraw)
		return;
	char buf[width()];
	sprint(buf);
	printf("%s", buf);
	fflush(stdout);
}

double ProgressBar::progress()
{
	return dperc;
}

void ProgressBar::show(double progress)
{
	update(progress);
	print();
}