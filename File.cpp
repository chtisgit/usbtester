#include "File.h"

#include <utility>
#include <stdexcept>
#include <cerrno>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

File::File(const char *path, int flags)
	: fd(open(path, flags)),eofbit(false)
{
	if(fd == -1){
		throw std::runtime_error("cannot open file "+std::string(path));
	}
}

File::File(File&& old)
	: fd(-1)
{
	std::swap(fd, old.fd);
}

File::~File()
{
	close();
}


off_t File::seek(off_t n, int whence)
{
	eofbit = false;
	return lseek(fd, n, whence);
}
off_t File::position()
{
	return seek(0, SEEK_SET);
}
off_t File::size()
{
	auto pos = position();
	auto sz = seek(0, SEEK_END);
	seek(pos);
	return sz;
}
std::pair<size_t,int> File::read(char *buf, size_t n)
{
	errno = 0;
	const char *orig = buf;
	do{
		ssize_t i = ::read(fd, buf, n);
		if(i == -1)
			goto end;
		buf += i;
		n -= i;
		if(i == 0){
			eofbit = true;
			break;
		}
	}while(n != 0);
end:
	return std::make_pair(size_t(buf-orig),errno);
}
std::pair<size_t,int> File::write(const char *buf, size_t n)
{
	errno = 0;
	const char *orig = buf;
	do{
		ssize_t i = ::write(fd, buf, n);
		if(i == -1)
			goto end;
		buf += i;
		n -= i;
	}while(n != 0);
end:
	return std::make_pair(size_t(buf-orig),errno);
}
int File::read_sector(size_t i, char *buf, size_t bs)
{
	auto pos = position();
	seek(i*bs);
	auto r = read(buf, bs);
	seek(pos);
	return r.second;
}
int File::write_sector(size_t i, const char *buf, size_t bs)
{
	auto pos = position();
	seek(i*bs);
	auto r = write(buf, bs);
	seek(pos);
	return r.second;
}
bool File::eof() {
	return eofbit;
}
void File::close()
{
	if(fd != -1){
		::close(fd);
		fd = -1;
	}
}
File::operator bool()
{
	return fd != -1;
}