#pragma once
#include <cstdlib>
#include <ctime>

class ProgressBar {
	time_t starttime;
	int perc;
	double dperc;
	size_t linewidth;

	bool started;
	bool redraw;

public:
	ProgressBar(size_t lw);

	size_t barwidth();
	size_t width();
	time_t eta();
	void sprinteta(char *buf, size_t n);
	void sprint(char *buf);
	void print(bool force = false);
	void update(double progress);
	void show(double progress);
	double progress();
};
