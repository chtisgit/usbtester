#pragma once
#include <utility>
#include <cstdlib>
#include <unistd.h>

class File {
	int fd;
	bool eofbit;
public:
	File(const char* path, int flags);
	File(const File&) = delete;
	File(File&&);
	~File();

	off_t seek(off_t n, int whence = SEEK_SET);
	off_t position();
	off_t size();
	std::pair<size_t,int> read(char *buf, size_t n);
	std::pair<size_t,int> write(const char *buf, size_t n);
	int read_sector(size_t i, char *buf, size_t bs);
	int write_sector(size_t i, const char *buf, size_t bs);
	bool eof();
	void close();
	operator bool();

};